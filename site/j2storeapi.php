<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_J2storeapi
 * @author     sp selvakumar <sp.selvakumar2012@gmail.com>
 * @copyright  2019 sp selvakumar
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\MVC\Controller\BaseController;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('J2storeapi', JPATH_COMPONENT);
JLoader::register('J2storeapiController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = BaseController::getInstance('J2storeapi');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
